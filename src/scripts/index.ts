

// import {axisBottom, axisLeft, json, max, scaleBand, scaleLinear, select} from 'd3'
//
// const margin = {left: 100, right: 10, top: 10, bottom: 150};
//
// const width = 600 - margin.left - margin.right,
//     height = 400 - margin.top - margin.bottom;
//
// const g = select("#canvas")
//     .append("svg")
//     .attr("width", width + margin.left + margin.right)
//     .attr("height", height + margin.top + margin.bottom)
//     .append("g")
//     .attr("transform", `translate(${margin.left}, ${margin.top})`);
//
// // X Label
// g.append("text")
//     .attr("class", "x axis-label")
//     .attr("x", width / 2)
//     .attr("y", height + 140)
//     .attr("font-size", "20px")
//     .attr("text-anchor", "middle")
//     .text("The word's tallest buildings");
//
// // Y Label
// g.append("text")
//     .attr("class", "y axis-label")
//     .attr("x", -(height / 2))
//     .attr("y", -60)
//     .attr("font-size", "20px")
//     .attr("text-anchor", "middle")
//     .attr("transform", "rotate(-90)")
//     .text("Height (m)");
//
// json("data/buildings.json").then((data: { name: string, height: number }[]) => {
//
//   data.forEach((d) => {
//     d.height = Number(d.height);
//   });
//
//   const x = scaleBand()
//       .domain(data.map(({name}) => name))
//       .range([0, width])
//       .paddingInner(0.3)
//       .paddingOuter(0.3);
//
//   const y = scaleLinear()
//       .domain([0, max(data, ({height}) => height)])
//       .range([height,0]);
//
//   const xAxisCall = axisBottom(x);
//   g.append("g")
//       .attr("class", "x axis")
//       .attr("transform", `translate(0, ${height})`)
//       .call(xAxisCall)
//       .selectAll("text")
//       .attr("y", "10")
//       .attr("x", "-5")
//       .attr("text-anchor", "end")
//       .attr("transform", "rotate(-40)");
//
//   const yAxisCall = axisLeft(y)
//       .ticks(3)
//       .tickFormat((d) => `${d}m`);
//   g.append("g")
//       .attr("class", "y-axis")
//       .call(yAxisCall);
//
//   const rects = g.selectAll("rect")
//       .data(data);
//
//   rects.enter()
//       .append("rect")
//       .attr("y", ({height}) => y(height))
//       .attr("x", ({name}) => x(name))
//       .attr("width", x.bandwidth)
//       .attr("height", (d) => height - y(d.height))
//       .attr("fill", "grey");
//
// });
