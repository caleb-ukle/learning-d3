import {axisBottom, axisLeft, interval, json, max, scaleBand, scaleLinear, select, transition} from 'd3'

const HEIGHT = 400;
const WIDTH = 600;
const MARGIN = {
  left: 100,
  right: 10,
  top: 10,
  bottom: 100
}
const INNER_HEIGHT = HEIGHT - MARGIN.top - MARGIN.bottom;

const INNER_WIDTH = WIDTH - MARGIN.left - MARGIN.right;
const t = transition().duration(300);

const y = scaleLinear()
    .range([INNER_HEIGHT, 0]);

const x = scaleBand()
    .range([0, INNER_WIDTH])
    .paddingInner(.3)
    .paddingOuter(.3);

const svg = select('#canvas').append('svg')
    .attr('width', WIDTH)
    .attr('height', HEIGHT)

const g = svg.append('g')
    .attr('transform', `translate(${MARGIN.left}, ${MARGIN.top})`);

const xLabel = g.append('text')
    .attr('class', 'x-axis-label')
    .attr('y', INNER_HEIGHT + 75)
    .attr('x', INNER_WIDTH / 2)
    .attr('font-size', '20px')
    .text('Time');

const yLabel = g.append('text')
    .attr('class', 'y-axis-label')
    .attr('x', -(INNER_HEIGHT / 2))
    .attr('y', -60)
    .attr('text-anchor', 'middle')
    .attr('font-size', '20px')
    .attr('transform', 'rotate(-90)')
    .text('Profit ($)')

const yAccessGroup = g.append('g')
    .attr('class', 'y-axis');

const xAxisGroup = g.append('g')
    .attr('class', 'x-axis')
    .attr('transform', `translate(0, ${INNER_HEIGHT})`)
// .selectAll('text')
// .attr('x', '-5')
// .attr('y', '5')
// .attr('text-anchor', 'end')
// .attr('transform', 'rotate(-50)');

let toggle = true;

json('/data/revenues.json')
    .then((data: { month: string, profit: number, revenue: number }[]) => {
      // parse data into correct types
      data.forEach(d => {
        d.profit = Number(d.profit);
        d.revenue = Number(d.revenue);
      })

      interval(() => {
        const newData = toggle ? data : data.slice(1);
        update(newData)
        toggle = !toggle;
      }, 1000 * 30)

      update(data);
    })

function update(data: { month: string, profit: number, revenue: number }[]) {

  const value = toggle ? 'profit' : 'revenue';

  y.domain([0, max(data, (d) => d[value])])
  x.domain(data.map(({month}) => month));

  const yAxisCall = axisLeft(y)
      .tickFormat(t => `$${t}`);

  const xAxisCall = axisBottom(x);

  yAccessGroup.transition(t).call(yAxisCall);

  xAxisGroup.transition(t).call(xAxisCall)

  // JOIN all data
  const bars = g.selectAll('rect')
      .data(data, (d: { month: string }) => d.month);

  // EXIT remove old data
  bars.exit()
      .attr('fill', 'red')
      .transition(t)
      .attr('y', y(0))
      .attr('height', 0)
      .remove();

  // UPDATE old elements present in new data
  bars
      .transition(t)
      .attr('y', (d) => y(d[value]))
      .attr('x', (d) => x(d.month))
      .attr('height', (d) => INNER_HEIGHT - y(d[value]))
      .attr('width', x.bandwidth())
  // .attr('fill', 'green')


  // ENTER new elements passed into data
  bars.enter()
      .append('rect')
      .attr('x', (d) => x(d.month))
      .attr('width', x.bandwidth())
      .attr('fill', 'green')
      .attr('y', y(0))
      .attr('height', 0)
      .transition(t)
      .attr('y', (d) => y(d[value]))
      .attr('height', (d) => INNER_HEIGHT - y(d[value]))

  yLabel
      .text(value)
}
