import {
  axisBottom,
  axisLeft,
  event as d3Event,
  format,
  interval,
  json,
  scaleLinear,
  scaleLog,
  scaleOrdinal,
  schemeCategory10,
  select,
  transition
} from 'd3'


const HEIGHT = 400;
const WIDTH = 600;
const MARGIN = {
  left: 100,
  right: 10,
  top: 10,
  bottom: 100
}
const INNER_HEIGHT = HEIGHT - MARGIN.top - MARGIN.bottom;

const INNER_WIDTH = WIDTH - MARGIN.left - MARGIN.right;
const t = transition().duration(100);
const y = scaleLinear()
    .domain([0, 90])
    .range([INNER_HEIGHT, 0]);

const x = scaleLog()
    .domain([300, 150000])
    .range([0, INNER_WIDTH])

const a = scaleLinear()
    .range([25 * Math.PI, 1500 * Math.PI])
    .domain([2000, 1400000000]);

const c = scaleOrdinal()
    .range(schemeCategory10)

const svg = select('#canvas').append('svg')
    .attr('width', WIDTH)
    .attr('height', HEIGHT)

const g = svg.append('g')
    .attr('transform', `translate(${MARGIN.left}, ${MARGIN.top})`);

const tooltip = select('body')
    .append('div')
    .attr('class', 'tooltip')
    .style('opacity', 0)

const legend = g.append('g')
    .attr('transform', `translate(${INNER_WIDTH - 20}, ${INNER_HEIGHT - 125})`)
    .attr('class', 'legend')

const xLabel = g.append('text')
    .attr('class', 'x-axis-label')
    .attr('y', INNER_HEIGHT + 75)
    .attr('x', INNER_WIDTH / 2)
    .attr('font-size', '20px')
    .attr("text-anchor", "middle")

const yLabel = g.append('text')
    .attr('class', 'y-axis-label')
    .attr('x', -(INNER_HEIGHT / 2))
    .attr('y', -60)
    .attr('text-anchor', 'middle')
    .attr('font-size', '20px')
    .attr('transform', 'rotate(-90)')
    .text('Life Expectancy')

const yAccessGroup = g.append('g')
    .attr('class', 'y-axis');

const xAxisGroup = g.append('g')
    .attr('class', 'x-axis')
    .attr('transform', `translate(0, ${INNER_HEIGHT})`)
// Axis Generators
const yAxisCall = axisLeft(y)

const xAxisCall = axisBottom(x)
    .tickValues([400, 4000, 40000])
    .tickFormat(format("$"));
const continents = ["africa", "americas", "asia", "europe"]
c.domain(continents);

continents.forEach((v, i) => {

  const row = legend.append('g')
      .attr('transform', `translate(0, ${i * 20})`);

  row.append('rect')
      .attr('width', 10)
      .attr('height', 10)
      .attr('fill', c(v))

  row.append('text')
      .attr('x', -10)
      .attr('y', 10)
      .attr('text-anchor', 'end')
      .style('text-transform', 'capitalize')
      .text(v)
})
// .selectAll('text')
// .attr('x', '-5')
// .attr('y', '5')
// .attr('text-anchor', 'end')
// .attr('transform', 'rotate(-50)');

interface ICountryData {
  countries: ICountry[]
  year: number;
}

interface ICountry {
  continent: string;
  country: string;
  income: null | number;
  life_exp: number | null;
  population: number;
}

json('/data/countries.json')
    .then(async (data: ICountryData[]) => {
      data.forEach(d => {
        d.year = Number(d.year);
      });

      const dataSet: ICountryData[] = data.map(d => {
            return {
              countries: d.countries.filter(c => !!c.income && !!c.life_exp),
              year: d.year,
            }
          }
      );

      let index = 0;
      interval(() => {
        update(dataSet[index % dataSet.length])
        index++
      }, 3000)
    });


function update(data: ICountryData) {
  // Build axis domains

  yAccessGroup.transition(t).call(yAxisCall);

  xAxisGroup.transition(t).call(xAxisCall)
  //
  // JOIN all data
  const bars = g.selectAll('circle')
      .data(data.countries, (d: ICountry) => d.country);

  // EXIT remove old data
  bars.exit()
      .attr('fill', 'gray')
      .transition(t)
      .attr('cy', y(0))
      .attr('r', 0)
      .remove();

  // UPDATE old elements present in new data
  bars
      .transition(t)
      .attr('cy', (d) => y(d.life_exp))
      .attr('cx', (d) => x(d.income))
      .attr('r', (d) => Math.sqrt(a(d.population) / Math.PI))


  // ENTER new elements passed into data
  bars.enter()
      .append('circle')
      .attr('cx', (d) => x(d.income))
      .attr('fill', (d) => c(d.continent))
      .attr('cy', y(0))
      .attr('r', 0)
      .on('mouseover', (d) => {
        tooltip
            .transition()
            .duration(200)
            .style('opacity', 0.9);
        tooltip
            .style('left', `${d3Event.pageX + 10}px`)
            .style('top', `${d3Event.pageY + 5}px`)
            .html(`
<p class="title">
<strong>${d.country}</strong>
</p>
<p class="body">
<br>
<ul>
<li>${d.continent}</li>
<li>${format("$,")(d.income)}</li>
<li>${d.life_exp}</li>
<li>${format(',')(d.population)}</li>
</ul>
</p>
`)
      })
      .on('mouseout', () => {
        tooltip
            .transition()
            .duration(250)
            .style('opacity', 0);
      })
      .transition(t)
      .attr('cy', (d) => y(d.life_exp))
      .attr('r', (d) => Math.sqrt(a(d.population) / Math.PI))
  xLabel.text(`GDP Per Capita ($) (${data.year})`)
}
