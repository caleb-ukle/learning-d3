# D3.js Follow Along Repo

> https://www.udemy.com/course/masteringd3js/

> Aim to port into Angular components

## How to use

1. Clone repo `git clone https://gitlab.com/caleb-ukle/learning-d3.git`
1. Move into directory `cd learning-d3`
1. Install dependencies `npm install`
1. Run demo `npm start` 


## Examples

### Live Update Bar Graph
> [Example Code](/src/scripts/01-barchart.ts)

![Updating-Line-Graph](https://media.calebukle.com/uploads/2020/04/wk-37VaHGCSom.gif)

### Live Update Scatter Plot
> [Example Code](/src/scripts/02-scatterplot.ts)

![Updating-scatter-plot](https://media.calebukle.com/uploads/Kapture%202020-04-03%20at%2021.05.50.gif)
